<?php

namespace VideoTargetBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('video_target');

        $rootNode
            ->children()
                ->scalarNode('uri')->isRequired()->end()
                ->arrayNode('auth')
                    ->children()
                        ->scalarNode('username')->isRequired()->end()
                        ->scalarNode('password')->isRequired()->end()
                        ->scalarNode('client_id')->isRequired()->end()
                        ->scalarNode('client_secret')->end()
                    ->end()
                ->end() // auth
                ->integerNode('cache_lifetime')->defaultValue(3600)->end()
                ->scalarNode('cache_directory')->defaultValue('%kernel.cache_dir%/vt')->end()
            ->end()
        ;
        return $treeBuilder;
    }
}